package io.gitlab.jfronny.gitea.helpdesk;

import java.util.LinkedList;
import java.util.List;

public class Config {
    public Gitea gitea;
    public EMail email;
    public Database database;
    public Web web;
    public Ban ban;

    public static class Gitea {
        public String host;
        public String token;

        public void validate() throws IllegalConfigException {
            if (host == null) throw new IllegalConfigException("Missing host in gitea");
            if (host.endsWith("/")) host = host.substring(0, host.length() - 1);
            if (token == null) throw new IllegalConfigException("Missing token in gitea");
        }
    }

    public static class EMail {
        public String address;
        public SMTP smtp;
        public POP3 pop3;
        public Integer waitTime;

        public static class SMTP {
            public String host;
            public String password;
            public Boolean ssl;

            public void validate() throws IllegalConfigException {
                if (host == null) throw new IllegalConfigException("Missing host in email/smtp");
                if (!Main.HOST_PATTERN.matcher(host).matches()) throw new IllegalConfigException("Illegal host in email/smtp");
                if (password == null) throw new IllegalConfigException("Missing password in email/smtp");
                if (ssl == null) throw new IllegalConfigException("Missing ssl in email/smtp");
            }
        }

        public static class POP3 {
            public String host;
            public String password;
            public Boolean ssl;

            public void validate() throws IllegalConfigException {
                if (host == null) throw new IllegalConfigException("Missing host in email/pop3");
                if (!Main.HOST_PATTERN.matcher(host).matches()) throw new IllegalConfigException("Illegal host in email/pop3");
                if (password == null) throw new IllegalConfigException("Missing password in email/pop3");
                if (ssl == null) throw new IllegalConfigException("Missing ssl in email/pop3");
            }
        }

        public void validate() throws IllegalConfigException {
            if (address == null) throw new IllegalConfigException("Missing address in email");
            if (!Main.MAIL_PATTERN.matcher(address).matches()) throw new IllegalConfigException("Illegal mail address");
            if (smtp == null) throw new IllegalConfigException("Missing smtp in email");
            smtp.validate();
            if (pop3 == null) throw new IllegalConfigException("Missing pop3 in email");
            pop3.validate();
            if (waitTime == null) throw new IllegalConfigException("Missing waitTime in email");
            if (waitTime < 0) throw new IllegalConfigException("Illegal wait time");
        }
    }

    public static class Database {
        public String connectionString;

        public void validate() throws IllegalConfigException {
            if (connectionString == null) throw new IllegalConfigException("Missing connectionString in database");
        }
    }

    public static class Web {
        public Integer port;
        public String publicAddress;

        public void validate() throws IllegalConfigException {
            if (port == null) throw new IllegalConfigException("Missing port in web");
            if (port <= 0) throw new IllegalConfigException("Illegal port");
            if (publicAddress == null) throw new IllegalConfigException("Missing publicAddress in web");
            if (publicAddress.endsWith("/")) publicAddress = publicAddress.substring(0, publicAddress.length() - 1);
        }
    }

    public static class Ban {
        public List<String> ignored;

        public void validate() {
            if (ignored == null) ignored = new LinkedList<>();
        }
    }

    public void validate() throws IllegalConfigException {
        if (gitea == null) throw new IllegalConfigException("Lacking gitea config");
        gitea.validate();
        if (email == null) throw new IllegalConfigException("Lacking gitea config");
        email.validate();
        if (database == null) throw new IllegalConfigException("Lacking gitea config");
        database.validate();
        if (web == null) throw new IllegalConfigException("Lacking web in config");
        web.validate();
        if (ban == null) ban = new Ban();
        ban.validate();
    }

    public static class IllegalConfigException extends Exception {
        public IllegalConfigException(String message) {
            super(message);
        }
    }
}
