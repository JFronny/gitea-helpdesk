package io.gitlab.jfronny.gitea.helpdesk.mail;

import jakarta.mail.MessagingException;

public class UncheckedMessagingException extends RuntimeException {
    private final MessagingException exception;

    public UncheckedMessagingException(MessagingException exception) {
        this.exception = exception;
    }

    public MessagingException getException() {
        return exception;
    }
}
