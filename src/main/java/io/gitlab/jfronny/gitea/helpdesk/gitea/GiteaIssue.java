package io.gitlab.jfronny.gitea.helpdesk.gitea;

public class GiteaIssue {
    public Long id;
    public String url;
    public String title;
    public String body;
    public String state;

    public static class Repository {
        public String full_name;
        public Long id;
        public String name;
        public String owner;
    }
}
