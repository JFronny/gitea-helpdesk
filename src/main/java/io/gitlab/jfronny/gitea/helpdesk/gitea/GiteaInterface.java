package io.gitlab.jfronny.gitea.helpdesk.gitea;

import io.gitlab.jfronny.commons.HttpUtils;
import io.gitlab.jfronny.gitea.helpdesk.Config;
import io.gitlab.jfronny.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;

public class GiteaInterface {
    private static final Type issueListType = new TypeToken<List<GiteaIssueComment>>() {}.getType();
    private final Config.Gitea gitea;

    public GiteaInterface(Config.Gitea gitea) {
        this.gitea = gitea;
    }

    public GiteaIssue getIssue(String owner, String repo, long issue) throws URISyntaxException, IOException {
        return HttpUtils.get(gitea.host + "/api/v1/repos/" + owner + "/" + repo + "/issues/" + issue)
                .header("Authorization", "token " + gitea.token)
                .sendSerialized(GiteaIssue.class);
    }

    public void closeIssue(String owner, String repo, long issue) throws URISyntaxException, IOException {
        HttpUtils.patch(gitea.host + "/api/v1/repos/" + owner + "/" + repo + "/issues/" + issue)
                .bodyJson("{\"state\":\"closed\"}")
                .header("Authorization", "token " + gitea.token)
                .send();
    }

    public GiteaIssue createIssue(String owner, String repo, String title, String body) throws URISyntaxException, IOException {
        CreateIssueOption opt = new CreateIssueOption();
        opt.title = title;
        opt.body = body;
        return HttpUtils.post(gitea.host + "/api/v1/repos/" + owner + "/" + repo + "/issues")
                .bodySerialized(opt)
                .header("Authorization", "token " + gitea.token)
                .sendSerialized(GiteaIssue.class);
    }

    public GiteaIssueComment addComment(String owner, String repo, long issue, String body) throws URISyntaxException, IOException {
        CreateIssueCommentOption opt = new CreateIssueCommentOption();
        opt.body = body;
        return HttpUtils.post(gitea.host + "/api/v1/repos/" + owner + "/" + repo + "/issues/" + issue + "/comments")
                .bodySerialized(opt)
                .header("Authorization", "token " + gitea.token)
                .sendSerialized(GiteaIssueComment.class);
    }

    public List<GiteaIssueComment> getComments(String owner, String repo, long issue) throws URISyntaxException, IOException {
        return HttpUtils.get(gitea.host + "/api/v1/repos/" + owner + "/" + repo + "/issues/" + issue + "/comments")
                .header("Authorization", "token " + gitea.token)
                .sendSerialized(issueListType);
    }

    public String getAddress() {
        return gitea.host;
    }
}
