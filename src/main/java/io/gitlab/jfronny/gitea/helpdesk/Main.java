package io.gitlab.jfronny.gitea.helpdesk;

import io.gitlab.jfronny.commons.HttpUtils;
import io.gitlab.jfronny.commons.log.Logger;
import io.gitlab.jfronny.commons.serialize.Serializer;
import io.gitlab.jfronny.commons.serialize.gson.api.v1.GsonHolders;
import io.gitlab.jfronny.gitea.helpdesk.db.DBInterface;
import io.gitlab.jfronny.gitea.helpdesk.gitea.GiteaInterface;
import io.gitlab.jfronny.gitea.helpdesk.mail.MailInterface;
import io.gitlab.jfronny.gitea.helpdesk.web.WebInterface;
import jakarta.mail.MessagingException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

// https://git.meli.delivery/meli/issue-bot
// https://www.javatpoint.com/example-of-receiving-attachment-with-email-using-java-mail-api
public class Main {
    public static final Logger LOG = Logger.forName("Gitea-Helpdesk");
    public static final Pattern MAIL_PATTERN = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])");
    public static final Pattern HOST_PATTERN = Pattern.compile("(?:[0-9a-zA-Z-._~]+|[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})(?::[0-9]{1,5})?");
    public static final Pattern PATH_SEGMENT_PATTERN = Pattern.compile("[a-zA-Z0-9-_]+");
    public static final Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");

    public static void main(String[] args) throws IOException, SQLException, MessagingException, Config.IllegalConfigException, InterruptedException {
        GsonHolders.registerSerializer();
        HttpUtils.setUserAgent("Gitea-Helpdesk/1.0");
        LOG.info("Running Gitea-Helpdesk");
        Config config;
        try (Reader r = Files.newBufferedReader(Path.of("config.json5"))) {
            config = Serializer.getInstance().deserialize(r, Config.class);
        }
        config.validate();
        GiteaInterface gitea = new GiteaInterface(config.gitea);
        try (DBInterface db = new DBInterface(config.database);
             MailInterface mail = new MailInterface(config.email);
             WebInterface web = new WebInterface(config.web, config.email.address, db, gitea)) {
            Predicate<String> ignore = config.ban.ignored
                    .stream()
                    .map(Pattern::compile)
                    .map(Pattern::asMatchPredicate)
                    .reduce(Predicate::or)
                    .orElse(s -> true);
            UpdateTask updateTask = new UpdateTask(db, mail, gitea, web, ignore);
            while (true) {
                updateTask.run();
                Thread.sleep(config.email.waitTime * 1000);
            }
        }
    }

    public static String getResource(String path) {
        try (InputStream is = Main.class.getResourceAsStream(path);
             InputStreamReader isr = new InputStreamReader(Objects.requireNonNull(is));
             BufferedReader br = new BufferedReader(isr)) {
            return br.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            LOG.error("Could not get resource: " + path);
            throw e;
        }
    }
}
