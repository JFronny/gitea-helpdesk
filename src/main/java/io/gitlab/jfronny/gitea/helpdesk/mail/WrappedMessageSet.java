package io.gitlab.jfronny.gitea.helpdesk.mail;

import jakarta.mail.Folder;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Iterator;

public class WrappedMessageSet implements Iterable<WrappedMessage>, AutoCloseable {
    private final Folder fld;
    private final String address;
    private final MailInterface mailInterface;

    public WrappedMessageSet(Folder fld, String address, MailInterface mailInterface) {
        this.fld = fld;
        this.address = address;
        this.mailInterface = mailInterface;
    }

    @Override
    public void close() throws MessagingException {
        fld.close();
    }

    @NotNull
    @Override
    public Iterator<WrappedMessage> iterator() {
        try {
            return Arrays.stream(fld.getMessages())
                    .map(s -> new WrappedMessage((MimeMessage) s, address, mailInterface))
                    .iterator();
        } catch (MessagingException e) {
            throw new UncheckedMessagingException(e);
        }
    }
}
