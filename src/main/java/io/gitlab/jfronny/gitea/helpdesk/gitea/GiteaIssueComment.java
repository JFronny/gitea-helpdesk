package io.gitlab.jfronny.gitea.helpdesk.gitea;

public class GiteaIssueComment {
    public String body;
    public Long id;
    public String issue_url;
    public String pull_request_url;
}
