package io.gitlab.jfronny.gitea.helpdesk.db;

public record Subscription(String id, String email, String repoOwner, String repo, long issue, long issueComment, String referenceChain, boolean creator) {
}
