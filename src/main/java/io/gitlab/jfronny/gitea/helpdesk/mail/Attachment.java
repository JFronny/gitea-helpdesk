package io.gitlab.jfronny.gitea.helpdesk.mail;

import jakarta.mail.MessagingException;

import java.io.IOException;
import java.io.InputStream;

public record Attachment(String fileName, InputStreamGenerator content) {
    public interface InputStreamGenerator {
        InputStream getInputStream() throws IOException, MessagingException;
    }
}
