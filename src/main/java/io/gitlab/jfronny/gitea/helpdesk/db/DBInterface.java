package io.gitlab.jfronny.gitea.helpdesk.db;

import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;
import io.gitlab.jfronny.gitea.helpdesk.Config;
import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.*;
import java.util.Optional;

public class DBInterface implements AutoCloseable {
    private final BasicDataSource ds = new BasicDataSource();

    public DBInterface(Config.Database config) throws SQLException {
        ds.setUrl("jdbc:" + config.connectionString);
        ds.setMinIdle(1);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
        ds.setAutoCommitOnReturn(true);
        ds.setDefaultAutoCommit(true);
        ds.start();
        try (Connection cx = ds.getConnection()) {
            try (Statement st = cx.createStatement()) {
                st.executeUpdate("""
                    create extension if not exists pgcrypto;
                    create or replace function helpdesk_generate_uid(size int) returns text as $$
                    declare
                      characters text := 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                      bytes bytea := gen_random_bytes(size);
                      l int := length(characters);
                      i int := 0;
                      output text := '';
                    BEGIN
                      while i < size loop
                        output := output || substr(characters, get_byte(bytes, i) % l + 1, 1);
                        i := i + 1;
                      end loop;
                      return output;
                    END;
                    $$ language plpgsql volatile;
                    create table if not exists helpdesk_subscriptions(
                        id text primary key default helpdesk_generate_uid(20),
                        email text,
                        repo_owner text,
                        repo text,
                        issue bigint,
                        issue_comment bigint,
                        reference_chain text,
                        creator bool
                    );""");
            }
        }
    }

    public <TEx extends Throwable> void forEachSubscription(ThrowingConsumer<Subscription, TEx> action) throws SQLException, TEx {
        try (Connection cx = ds.getConnection();
             Statement st = cx.createStatement();
             ResultSet rs = st.executeQuery("select * from helpdesk_subscriptions")) {
            while (rs.next()) {
                action.accept(get(rs));
            }
        }
    }

    public String addSubscription(String email, String repoOwner, String repo, long issue, long issueComment, String referenceChain, boolean creator) throws SQLException {
        try (Connection cx = ds.getConnection();
             PreparedStatement st = cx.prepareStatement("""
                    insert into helpdesk_subscriptions (email, repo_owner, repo, issue, issue_comment, reference_chain, creator)
                    values (?, ?, ?, ?, ?, ?, ?)
                    returning id""")) {
            st.setString(1, email);
            st.setString(2, repoOwner);
            st.setString(3, repo);
            st.setLong(4, issue);
            st.setLong(5, issueComment);
            st.setString(6, referenceChain);
            st.setBoolean(7, creator);
            try (ResultSet rs = st.executeQuery()) {
                rs.next();
                return rs.getString("id");
            }
        }
    }

    public Optional<Subscription> getSubscription(String id) throws SQLException {
        try (Connection cx = ds.getConnection();
             PreparedStatement st = cx
                .prepareStatement("select id, email, repo_owner, repo, issue, issue_comment, reference_chain, creator from helpdesk_subscriptions where id = ?")) {
            st.setString(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(get(rs));
                } else return Optional.empty();
            }
        }
    }

    public void removeSubscription(String id) throws SQLException {
        try (Connection cx = ds.getConnection();
             PreparedStatement st = cx.prepareStatement("delete from helpdesk_subscriptions where id = ?")) {
            st.setString(1, id);
            st.executeUpdate();
        }
    }

    public void updateSubscriptionIssueComment(String id, long issueComment) throws SQLException {
        try (Connection cx = ds.getConnection();
             PreparedStatement st = cx.prepareStatement("update helpdesk_subscriptions set issue_comment = ? where id = ?")) {
            st.setLong(1, issueComment);
            st.setString(2, id);
            st.executeUpdate();
        }
    }

    public void updateSubscriptionReferenceChain(String id, String referenceChain) throws SQLException {
        try (Connection cx = ds.getConnection(); PreparedStatement st = cx.prepareStatement("update helpdesk_subscriptions set reference_chain = ? where id = ?")) {
            st.setString(1, referenceChain);
            st.setString(2, id);
            st.executeUpdate();
        }
    }

    private Subscription get(ResultSet rs) throws SQLException {
        return new Subscription(
                rs.getString("id"),
                rs.getString("email"),
                rs.getString("repo_owner"),
                rs.getString("repo"),
                rs.getLong("issue"),
                rs.getLong("issue_comment"),
                rs.getString("reference_chain"),
                rs.getBoolean("creator")
        );
    }

    @Override
    public void close() throws SQLException {
        ds.close();
    }
}
