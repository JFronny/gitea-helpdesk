package io.gitlab.jfronny.gitea.helpdesk;

public class UnexpectedMailException extends Exception {
    public UnexpectedMailException(String message) {
        super(message);
    }
}
