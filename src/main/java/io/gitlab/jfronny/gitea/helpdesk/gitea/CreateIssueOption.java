package io.gitlab.jfronny.gitea.helpdesk.gitea;

public class CreateIssueOption {
    public String body;
    public String title;
}
