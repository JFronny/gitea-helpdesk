plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

application {
    mainClass.set("io.gitlab.jfronny.gitea.helpdesk.Main")
}

group = "io.gitlab.jfronny"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/35745143/packages/maven")
}

dependencies {
    implementation("com.sun.mail:jakarta.mail:2.0.1")
    implementation("org.postgresql:postgresql:42.5.0")
    implementation("org.apache.commons:commons-dbcp2:2.9.0")
    implementation("io.gitlab.jfronny:commons:2022.10.22+20-29-33")
    implementation("io.gitlab.jfronny:commons-gson:2022.10.22+20-29-33")
    implementation("com.kohlschutter.junixsocket:junixsocket-core:2.6.0")
    implementation("org.jsoup:jsoup:1.15.3")
    implementation("net.freeutils:jlhttp:2.6")
    implementation("org.commonmark:commonmark:0.20.0")
    compileOnly("org.jetbrains:annotations:23.0.0")
}
