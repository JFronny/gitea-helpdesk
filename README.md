# Gitea-Helpdesk
Gitea-Helpdesk is an extension to Gitea that allows users to create issues through emails, comment on existing ones and receiving notifications for new comments.
To do so, it must be connected to a postgresql database, a gitea instance and a mail server (pop3+smtp).
The mail server MUST support sub-addressing with `+` and the repository name and gitea username must match `[a-zA-Z0-9-_]+`.
To set up the service, build a jar and add a config file as follows to the current directory:
```json5
{
  "gitea": {
    "host": "https://gitea.example.com", // Your gitea instance 
    "token": "0123456789abcdef0123456789abcdef12345678" // An access token
  },
  "email": {
    "address": "helpdesk@example.com", // The e-mail address of the help desk
    "waitTime": 10, // The amount of seconds to wait between each iteration
    "smtp": {
      "host": "smtp.example.com:465", // The address of the SMTP server
      "password": "1234", // The password for login (the username is the address)
      "ssl": true // Whether to enable ssl encryption
    },
    "pop3": {
      "host": "pop3.example.com:995", // The address of the POP3 server
      "password": "1234", // The password for login (the username is the address)
      "ssl": true // Whether to enable ssl encryption
    }
  },
  "database": {
    "connectionString": "postgresql://localhost/helpdesk?user=helpdesk" // The postgresql connection string (see postgresql jdbc documentation, sockets are supported)
  },
  "web": {
    "port": 80, // The port to host the website at
    "publicAddress": "http://127.0.0.1:80" // Where users will access the website
  }
}
```
It is also recommended to adjust the template files used in Gitea to reference the Helpdesk for users.
The templates used on my own instance can be found in templates/